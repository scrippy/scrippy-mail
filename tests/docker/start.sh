#!/bin/sh
case $1 in
  init)
    echo "Init..."
    ;;
  start)
    echo "Starting Dovecot..."
    dovecot -c /etc/dovecot/dovecot.conf
    echo "Starting Postfix..."
    /usr/sbin/postfix -v start-fg
esac
