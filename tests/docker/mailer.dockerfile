FROM alpine:latest
RUN apk add shadow bash openssl postfix dovecot dovecot-pop3d
COPY start.sh .
RUN chmod +x ./start.sh
RUN ./start.sh init

COPY ./dovecot/ /etc/dovecot/
RUN groupadd -g 1000 harry.fink
RUN groupadd -g 1001 luiggi.vercotti
RUN useradd -m -d /home/harry.fink -s /bin/bash -u 1000 -g 1000 harry.fink
RUN usermod --password $(echo -n 0123456789 | openssl passwd -1 -stdin) harry.fink
RUN useradd -m -d /home/luiggi.vercotti -s /bin/bash -u 1001 -g 1001 luiggi.vercotti
RUN usermod --password $(echo -n 0123456789 | openssl passwd -1 -stdin) luiggi.vercotti
RUN rm /var/mail/harry.fink /var/mail/luiggi.vercotti
RUN mkdir -p /var/mail/harry.fink /var/mail/luiggi.vercotti
RUN chown 1000: /var/mail/harry.fink
RUN chown 1001: /var/mail/luiggi.vercotti

COPY ./postfix/main.cf /etc/postfix/
COPY ./postfix/master.cf /etc/postfix/
COPY ./postfix/aliases /etc/aliases
RUN newaliases -oA/etc/aliases

ENTRYPOINT ["./start.sh", "start"]
EXPOSE 2500
EXPOSE 110
