scrippy\_mail package
=====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   scrippy_mail.ciphers
   scrippy_mail.files
   scrippy_mail.imap
   scrippy_mail.pop
   scrippy_mail.smtp

Module contents
---------------

.. automodule:: scrippy_mail
   :members:
   :undoc-members:
   :show-inheritance:
